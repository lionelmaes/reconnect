import { createStore } from 'vuex'
import axios from 'axios'


export default createStore({
  state: {
      status:'',
      token: localStorage.getItem('token') || '',
      user : {}
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, token, user){
      state.status = 'success',
      state.token = token,
      state.user = user
    },
    auth_error(state){
      state.status = error
    },
    logout(state){
      state.status = '',
      state.token = ''
    }
  },

  actions: {
    login({commit}, email, password){
      commit('auth_request');
      try{
        const res = await axios.post('http://lvh2.com:1337/auth/local', {
          identifier:email, 
          password:password
        });
        const {jwt, user} = res.data;
        localStorage.setItem('token', jwt);
        axios.defaults.headers.common['Authorization'] = jwt;
        commit('auth_success', jwt, user);

        
      }catch(err){
        commit('auth_error');
        localStorage.removeItem('token');
        reject(err);
      }
    }


  },
  getters : {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status
  },
  modules: {
  }
})
