
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//import VueProgress from 'vue-progress-path'
//import 'vue-progress-path/dist/vue-progress-path.css'
import './assets/css/style.css'

createApp(App).use(store).use(router).mount('#app')
