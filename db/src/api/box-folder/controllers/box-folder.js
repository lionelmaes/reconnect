'use strict';

/**
 *  box-folder controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::box-folder.box-folder');
