'use strict';

/**
 * box-folder service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::box-folder.box-folder');
