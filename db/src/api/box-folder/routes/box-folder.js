'use strict';

/**
 * box-folder router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::box-folder.box-folder');
