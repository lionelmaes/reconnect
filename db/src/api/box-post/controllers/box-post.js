'use strict';

/**
 *  box-post controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::box-post.box-post');
