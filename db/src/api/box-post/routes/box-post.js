'use strict';

/**
 * box-post router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::box-post.box-post');
