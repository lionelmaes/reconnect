'use strict';

/**
 * box-post service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::box-post.box-post');
